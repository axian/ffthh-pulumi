# About the Dev Container

The Dockerfile builds everything that is needed to develop inside of a container. Development in a container provides these benefits:

* Keeps your host system clean.
* Dev setup requirements are easily transferrable between hosts.
* Version isolation for terraform, aws cli, python, etc.
* Allows you to configure AWS credentials in the container itself.

This container includes:

* dotnet core 3.1
* AWS CLI tools
* Pulumi (latest)

## First Time Spinup

After spinning up VS Code for the first time, it will ask you to re-open the folder in a container. If you miss the prompt, you can also click the green arrows in the lower left and choose to open the project inside the container.

First run is going to take some time. It needs to build from the Dockerfile, plus install VS Code extensions inside the container.

Once complete, you will see a bash command in the terminal that looks like this:

`vscode ➜ /workspaces/pulumi-aws (master) $`

Try out a few commands to make sure that they are installed and the commands are accessible:

TODO: Add PULUMI command lines

## Configure AWS Credentials

You will need to set your AWS credentials that point to Axian L&D, or whatever AWS subscription you are developing in.

1. Generate a new API Key in the AWS Console.
1. Open VS Code and run in the dev container.
1. In the terminal, use `aws configure` to set the API Key.
1. To validate you are pointed at the correct subscription, use: `aws sts get-caller-identity`

## Persist Environment Variables

TODO: May not be needed

If you need to persist specific environment variables in your container, follow these steps.

1. Open a terminal and run: `nano ~/.bashrc`
1. Add export statements as needed to the bottom of the script. Example:

   ```txt
   export GITHUB_OWNER=NLSInc
   export GITHUB_TOKEN=123456
   export SOME_ENV_VAR=SOME_VALUE
   ```

1. Hit `ctrl-o` then `enter` to write
1. Exit nano
1. Close all terminals and reopen
1. To check, run `printenv`

NOTE: Be sure to close out of all other open terminals. Env vars will not refresh automatically.
