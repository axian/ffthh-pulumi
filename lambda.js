exports.handler = async (event) => {
    const response = {
        statusCode: 200,
        body: JSON.stringify('{RETURN_VALUE}'),
    };
    return response; 
};